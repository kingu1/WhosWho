#!/usr/bin/python3

# Builds translations from .po files before running setup.py.
# Certainly not the proper way to do it, but better than nothing.

import glob, os, shutil, subprocess

po_dir = "po"
pot_file = os.path.join("po", "whoswho.pot")
output_dir = "built_translations"

#if os.path.exists(output_dir):
#    clean = input("Are you OK to remove output directory (" + output_dir +")? (N/y)")
#    if clean == "y":
#        shutil.rmtree(output_dir)
#    else:
#        print("Aborting")
#        exit()

print("Building .mo files:")
for po_file in glob.glob("%s/*.po" % po_dir):
    lang = os.path.basename(po_file[:-3])
    print("- " + lang)
    mo_dir = os.path.join(output_dir, "mo", lang, "LC_MESSAGES")
    mo_file = os.path.join(mo_dir, "whoswho.mo")
    if not os.path.exists(mo_dir):
        os.makedirs(mo_dir)
    cmd = ["msgfmt", po_file, "-o", mo_file]
    subprocess.run(cmd)

print("Building .desktop and .metainfo.xml files")
for (template, switch, file_merged) in [("data/fr.masson_informatique.WhosWho.desktop.template",
                                         "--desktop",
                                         "fr.masson_informatique.WhosWho.desktop"),
                                        ("data/fr.masson_informatique.WhosWho.metainfo.xml",
                                         "--xml",
                                         "fr.masson_informatique.WhosWho.metainfo.xml")] :
    output = os.path.join("built_translations", file_merged)
    cmd = ["msgfmt", switch, "--template", template, "-d", po_dir,
           "-o", output]
    subprocess.run(cmd)
